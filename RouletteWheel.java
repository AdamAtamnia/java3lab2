import java.util.Random;

public class RouletteWheel{
    private Random rand;
    private int number = 0;

    public RouletteWheel(){
        rand = new Random();
    }
    
    public void spin(){
        number = rand.nextInt(38);
    }

    public int getValue(){
        return this.number;
    }


}